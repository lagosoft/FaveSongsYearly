import { h, Component } from 'preact';
import style from './style';

const fakeData = [
	{
		year: 2013,
		song: 'Remember',
		artist: 'Dead Mau5',
		yearReleased: 2009,
		genre: 'Progressive House'
	},
	{
		year: 2014,
		song: 'Ogodo ano 2000',
		artist: 'Tom Zé',
	},
	{
		year: 2015,
		song: 'Inspector Norse',
		artist: 'Todd Trejer',
	},
	{
		year: 2016,
		song: 'Conmosion',
		artist: 'Chimo Psicodelico',
	},
	{
		year: 2017,
		song: 'Lo Dudo',
		artist: 'Frankie Ruiz (composer: Jose Jose)',
	},
]

export default class Home extends Component {

	constructor(props){
		super(props)
		this.state = {
			faveSongsYearly: []
		}
		console.log("constructor")
	}

	componentWillMount(){
		console.log("componentWillMount")
		this.setState({
			faveSongsYearly: fakeData
		})
	}

	getFaveSongElements(){
		let content = '';
		for( let song of this.state.faveSongsYearly){
			console.log("Got SSSONGg: ", song.year)
			content += (<div>{song.year}</div>)
		}
		console.log("rend first", content);
		return (<div>{content}</div>);
	}

	render() {

		let content = this.getFaveSongElements()
		console.log("render", content);
		return (
			<div class={style.home}>
				{content} 
			</div>
		);
	}
}
