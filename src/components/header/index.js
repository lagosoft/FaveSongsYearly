import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';

const Header = () => {
	return (
		<header class={style.header}>
			<h1>Fabio Valentino's</h1>
			<h5>fave songs per year</h5>
			<nav>
				<Link activeClassName={style.active} href="/">Home</Link>
			</nav>
		</header>
	);
};

export default Header;
